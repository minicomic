#
# minicomic.rb: Rake tasks for minicomics using Inkscape and other tools
#
# Copyright 2007  MenTaLguY <mental@rydia.net>
# Copyright 2007  John Bintz <jcoswell@coswellproductions.org>
#
# This library is made available under the same terms as Ruby.
#

require 'set'
require 'rake'
require 'rexml/document'
require 'rexml/streamlistener'
require 'yaml'

class Minicomic

DEFAULT_OPTIONS = {
  :rasterize => true,

  :margin => 13.5,
  :dpi => 200,

  :color => false,

  :web_format => :png,

  :paper => :letter,
  :orientation => :landscape,

  # in pixels
  :web_height => 680,
  :thumbnail_height => 96,
  :web_jpeg_quality => 75,
  :generate_thumbnails => true,

  :half_size => false,
  :force_dpi => false
}

private

# constants
LEFT = 0
RIGHT = 1
PHI = ( 1 + Math.sqrt( 5 ) ) / 2

module Common
  def get_ps_bbox( filename )
    result = nil
    File.open( filename, "r" ) do |stream|
      magic = stream.gets.chomp
      break unless magic =~ /^%!PS-Adobe/
      stream.each_line do |line|
        case line
        when /^%%BoundingBox: (\d+) (\d+) (\d+) (\d+)/
          result = [ ($1.to_f)..($3.to_f), ($2.to_f)..($4.to_f) ]
          break
        when /^%%EndComments/
          break
        end
      end
    end
    raise RuntimeError, "unable to get bbox for #{ filename }" unless result
    result
  end

  def get_generic_bbox( filename )
    IO.popen("-", "r") do |stream|
      if stream
        line = stream.gets
        if line
          line.chomp.split.map { |n| 0.0..(n.to_f) }
        else
          raise RuntimeError, "unable to get bbox for #{ filename }"
        end
      else
        begin
          sh "identify", "-format", "%w %h", filename
        rescue Exception => e
          e.display
        end
        exit! 127
      end
    end
  end

  class SVGBBoxListener
    include REXML::StreamListener
    
    def initialize
      @bbox = nil
    end
    
    def tag_start(name, attrs)
      case name
      when "svg"
        @bbox = [ 0..(attrs['width'].to_f), 0..(attrs['height'].to_f) ]
        throw :got_bbox, true
      end  
    end
    
    def bbox
      raise "Unable to obtain bbox" unless @bbox
      @bbox
    end
  end

  def get_svg_bbox( filename )
    bbox_listener = SVGBBoxListener.new
    catch(:got_bbox) do
      File.open( filename, "r" ) do |stream|
        REXML::Document.parse_stream( stream, bbox_listener )
      end
    end
    bbox_listener.bbox
  end

  def get_bbox( filename )
    case filename
    when /\.svg$/i
      get_svg_bbox( filename )
    when /\.e?ps$/i
      get_ps_bbox( filename )
    else
      get_generic_bbox( filename )
    end
  end

  def scale_print_page( width, height )
    width_scale = @options[:page_width].to_f / width
    height_scale = @options[:page_height].to_f / height
    if width * height_scale > @options[:page_width]
      width_scale
    else
      height_scale
    end * @options[:scale]
  end
end
include Common

def eps_from_svg( eps_file, svg_file )
  file eps_file => [ svg_file ] do
    sh 'inkscape', '-T', '-B', '-E', eps_file, svg_file
  end
end

def back_ps( out_file, in_file )
  file out_file => [ in_file ] do
    sh 'psselect', '-e', in_file, out_file
  end
end

def front_ps( out_file, in_file )
  file out_file => [ in_file ] do
    sh 'psselect', '-o', in_file, out_file
  end
end

def make2up( in_file, out_file )
  page_width = @options[:page_width].round
  page_height = @options[:page_height].round
  case @options[:orientation]
  when :landscape
    if @options[:half_size]
      layout = "2:0L(#{page_height},0)+1L(#{page_height},#{page_width})+0L(#{page_height*2},0)+1L(#{page_height*2},#{page_width})"
    else
      layout = "2:0L(#{page_height},0)+1L(#{page_height},#{page_width})"
    end
  else
    if @options[:half_size]
      layout = "2:0(0,0)+1(#{page_width},0)+0(0,#{page_height})+1(#{page_width},#{page_height})"
    else
      layout = "2:0(0,0)+1(#{page_width},0)"
    end
  end
  sh 'pstops', "-w#{ @options[:paper_width].round }", "-h#{ @options[:paper_height].round }", layout, in_file, out_file
end

def duplex_ps( out_file, in_file )
  temp_file = "#{ out_file }.temp"
  file out_file => [ in_file ] do
    sh 'psbook', in_file, temp_file
    begin
      make2up temp_file, out_file
    ensure
      rm temp_file
    end
  end
end

def proof_ps( out_file, in_file )
  temp_file = "#{ out_file }.temp"
  file out_file => [ in_file ] do
    sh 'psselect', '-p_1,1-_2', in_file, temp_file
    begin
      make2up temp_file, out_file
    ensure
      rm temp_file
    end
  end
end

def pdf_from_ps( pdf_file, ps_file )
  file pdf_file => [ ps_file ] do
    case @options[:orientation]
    when :landscape
      orient = 3
    else
      orient = 0
    end
      
    sh 'gs', '-q', '-dSAFER', '-dNOPAUSE', '-dBATCH', '-sDEVICE=pdfwrite', "-sOutputFile=#{ pdf_file }", '-dAutoRotatePages=/None', '-c', "<< /PageSize [#{ @options[:paper_width].round } #{ @options[:paper_height].round }] /Orientation #{orient} >> setpagedevice", '-c', '.setpdfwrite', '-f', ps_file
  end
end

def png_from_svg( png_file, svg_file, high_res, &height_calc )
  png_from_file( png_file, svg_file, high_res, true, &height_calc )
end

def png_from_image_file( png_file, image_file, high_res, &height_calc )
  png_from_file( png_file, image_file, high_res, false, &height_calc )
end

def jpeg_from_image_file( jpeg_file, image_file )
  file jpeg_file => [ image_file ] do
    sh 'convert', '-quality', @options[:web_jpeg_quality].to_s, image_file, jpeg_file
  end
end

def png_from_file( png_file, image_file, high_res, is_svg, &height_calc )
  temp_png_file = "#{ png_file }.uncrushed"
  file png_file => [ image_file ] do
    height = height_calc.call image_file
    
    if is_svg
      sh 'inkscape', '-h', height.to_s, '-C', '-y', '1.0', '-e', temp_png_file, image_file
    else
      if @options[:force_dpi]
        sh 'convert', image_file, "-filter", "Lanczos", "-resize", "x#{height}>", "png:#{temp_png_file}"
      else
        sh 'convert', image_file, "png:#{temp_png_file}"
      end
    end
    
    begin
      args = '-q', '-rem', 'allb'
      case @options[:color]
      when :color, true
        args.push '-c', '2'
      when :greyscale, :grayscale, false
        args.push '-c', '0', '-bit_depth', '8'
      when :monochrome
        args.push '-c', '0', '-bit_depth', ( high_res ? '1' : '4' )
      end
      args.push temp_png_file, png_file
      sh 'pngcrush', *args
    rescue Exception
      rm png_file if File.exist? png_file
      raise
    ensure
      rm temp_png_file
    end
  end
end

def web_png_from_svg( png_file, svg_file )
  png_from_svg( png_file, svg_file, false ) { @options[:web_height] }
end

def web_png_from_image_file( png_file, image_file )
  png_from_image_file( png_file, image_file, false ) { @options[:web_height] }
end

def print_png_from_svg( png_file, splits, svg_file )
  png_from_svg( png_file, svg_file, @options[:dpi] > 150 ) do |filename|
    dims = get_bbox( filename ).map { |r| r.end - r.begin }
    scale = scale_print_page( dims[0] / splits, dims[1] )
    ( dims[1].to_f * scale * @options[:dpi] / 72 ).round
  end
end

def print_png_from_image_file( png_file, splits, image_file )
  png_from_image_file( png_file, image_file, @options[:dpi] > 150 ) do |filename|
    dims = get_bbox( filename ).map { |r| r.end - r.begin }
    scale = scale_print_page( dims[0] / splits, dims[1] )
    ( dims[1].to_f * scale * @options[:dpi] / 72 ).round
  end
end

def print_eps_from_image_file( eps_file, splits, image_file )
  eps_from_image_file( eps_file, image_file ) do |filename|
    dims = get_bbox( filename ).map { |r| r.end - r.begin }
    scale = scale_print_page( dims[0] / splits, dims[1] )
    ( dims[1].to_f * scale * @options[:dpi] / 72 ).round
  end
end

def thumbnail_jpeg_from_image( jpeg_file, image_file )
  file jpeg_file => [ image_file ] do
    sh 'convert', '-filter', 'sinc', '-resize', "x#{ @options[:thumbnail_height] }", image_file, jpeg_file
  end
end

ImageSlice = Struct.new :n, :total, :filename
PageSpec = Struct.new :bbox, :scale, :dims, :filename

class BookletLayout
  include Common

  def initialize( options, stream, *pages )
    @options = options
    @bboxes = {}
    @stream = stream

    pages.push nil if pages.size % 2 != 0 # pad to even number of pages

    # move the back cover to the beginning, to be paired with the front cover
    pages.unshift pages.pop

    # pair up adjacent pages into spreads
    spreads = (0...(pages.size/2)).map { |i| [ pages[i*2], pages[i*2+1] ] }

    # format each spread, breaking spreads back into invdividual pages after
    pages = spreads.inject( [] ) do |acc, spread|
      is_cover = acc.empty?
      acc.push *format_pages( is_cover, *spread )
    end

    # return the back cover to the end
    pages.push pages.shift

    emit_document pages
  end

  def format_pages( is_cover, *pages )
    spread = pages.map do |page|
      if page
        bounds = bbox( page )
        dims = (0..1).map { |d| bounds[d].end - bounds[d].begin }
        scale = scale_print_page( *dims )
        PageSpec[ bounds, scale, dims.map { |n| n * scale }, page.filename ]
      else
        nil
      end
    end

    if spread.all? # i.e. both?
      spread_dims = spread.map { |page| page.dims }

      if spread[LEFT].filename == spread[RIGHT].filename
        tx = translate_spread( *spread_dims )
      elsif is_cover
        tx = spread_dims.map { |dims| translate_single( dims ) }
      else
        tx = translate_facing( *spread_dims )
      end

      [ LEFT, RIGHT ].map do |side|
        [ spread[side].bbox, spread[side].scale, tx[side],
          spread[side].filename ]
      end
    else
      spread.map do |page|
        if page
          [ page.bbox, page.scale, translate_single( page.dims ),
            page.filename ]
        else
          nil
        end
      end
    end
  end

  def bbox( page )
    rx, ry = @bboxes[page.filename] ||= get_bbox( page.filename )
    width = rx.end - rx.begin
    [ ( rx.begin + width * page.n / page.total )..( rx.begin + width * ( page.n + 1 ) / page.total ), ry ]
  end

  def bottom_margin( height )
    ( @options[:page_height].to_f - height ) / PHI
  end

  def horizontal_margin( width )
    ( @options[:page_width].to_f - width ) / 2
  end

  def translate_single( dims )
    [ horizontal_margin( dims[0] ), bottom_margin( dims[1] ) ]
  end

  def translate_facing( left, right )
    left_margin = horizontal_margin( left[0] )
    right_margin = horizontal_margin( right[0] )
    gutter = [ [ left_margin, right_margin ].min, 0 ].max
    [ [ left_margin + ( gutter / 3 ), bottom_margin( left[1] ) ],
      [ right_margin - ( gutter / 3 ), bottom_margin( right[1] ) ] ]
  end

  def translate_spread( left, right )
    [ [ @options[:page_width].to_f - left[0], bottom_margin( left[1] ) ],
      [ 0, bottom_margin( right[1] ) ] ]
  end

  def emit_dsc( name, value=nil )
    if value
      @stream.puts "%%#{ name }: #{ value }"
    else
      @stream.puts "%%#{ name }"
    end
  end

  def emit_document( pages )
    @stream.puts "%!PS-Adobe-3.0"
    emit_dsc 'Creator', 'minicomic'
    emit_dsc 'Pages', pages.size
    #emit_dsc 'Orientation', 'Portrait'
    #bbox = "0 0 #{ @options[:paper_width].round } #{ @options[:paper_height].round }"
    #emit_dsc 'BoundingBox', bbox
    #emit_dsc 'HiResBoundingBox', bbox
    emit_dsc 'EndComments'
    pages.each_with_index do |page, n|
      if page
        emit_page( n, *page )
      else
        emit_empty_page( n )
      end
    end
    emit_dsc 'EOF'
  end

  def emit_clip_rect( x0, y0, x1, y1 )
    @stream.puts "newpath"
    @stream.puts "#{ x0 } #{ y0 } moveto"
    @stream.puts "#{ x0 } #{ y1 } lineto"
    @stream.puts "#{ x1 } #{ y1 } lineto"
    @stream.puts "#{ x1 } #{ y0 } lineto"
    @stream.puts "closepath eoclip newpath"
  end

  def emit_page( n, bbox, scale, translate, document )
    emit_dsc 'Page', "#{ n } #{ n }"

    @stream.puts "save"
    @stream.puts "/showpage {} def"
    emit_clip_rect( 0, 0, @options[:page_width].round, @options[:page_height].round )

    @stream.puts <<EOS
#{ translate.join( ' ' ) } translate
#{ scale } #{ scale } scale
EOS

    emit_embedded_page( bbox, document )

    @stream.puts "restore"
    @stream.puts "showpage"
    emit_dsc 'PageTrailer'
  end

  def emit_embedded_page( bbox, document )
    case document
    when /\.e?ps$/i
      emit_embedded_page_eps( bbox, document )
    else
      emit_embedded_page_generic( bbox, document )
    end
  end

  def emit_embedded_page_eps( bbox, document )
    @stream.puts "#{ bbox.map { |r| -r.begin }.join( ' ' ) } translate"
    emit_clip_rect( bbox[0].begin, bbox[1].begin, bbox[0].end, bbox[1].end )
    emit_dsc 'BeginDocument', File.basename( document )
    File.open( document, 'r' ) do |input|
      input.each_line do |line|
        @stream.puts line
      end
    end
    emit_dsc 'EndDocument'
  end

  def emit_embedded_page_generic( bbox, document )
    emit_dsc 'BeginDocument', File.basename( document )
    IO.popen( "-", "r" ) do |input|
      if input
        input.each_line do |line|
          @stream.puts line
        end
      else
        begin
          geometry = "#{ ( bbox[0].end - bbox[0].begin ).round }x#{ ( bbox[1].end - bbox[1].begin ).round }"
          # straightforward image -> eps conversion doesn't work for spreads, write to temp png to work around this
          temp_png_image = "#{document}.intermediate"
          sh "convert", "-crop", "#{geometry}+#{ bbox[0].begin.round }+#{ bbox[1].begin.round }", "+repage", document, "png:#{temp_png_image}"
          sh "convert", "png:#{temp_png_image}", "+repage", "eps:-"
          rm temp_png_image
        rescue Exception => e
          e.display
        end
        exit! 127
      end
    end
    emit_dsc 'EndDocument'
  end

  def emit_empty_page( n )
    emit_dsc 'Page', "#{ n } #{ n }"
    @stream.puts "showpage"
  end
end

def layout_booklet( layout_ps, *pages )
  file layout_ps => pages.compact.map { |page| page.filename } do
    begin
      File.open( layout_ps, 'w' ) do |stream|
        BookletLayout.new( @options, stream, *pages )
      end
    rescue
      rm layout_ps
      raise
    end
  end
end

SPECIAL_NAMES = %w(front-cover back-cover cover)
SPECIAL_NAME_RES = SPECIAL_NAMES.map do |name|
  Regexp.new( Regexp.quote( name ).gsub( /-/, "\W*" ), "i" )
end

def normalize_special_name( name )
  name = name.gsub( /[a-z](?=[A-Z])/, '\&-' )
  name.gsub!( /\W+/, '-' )
  name.downcase!
  name
end

def initialize( dir, options={} )
  @options = DEFAULT_OPTIONS.merge options

  case @options[:paper]
  when :letter
    @options[:paper_width] = 8.5 * 72
    @options[:paper_height] = 11 * 72
  when :legal
    @options[:paper_width] = 8.5 * 72
    @options[:paper_height] = 14 * 72
  when :tabloid
    @options[:paper_width] = 11 * 72
    @options[:paper_height] = 17 * 72
  when :a4
    @options[:paper_width] = 210 * 72 / 25.4
    @options[:paper_height] = 297 * 72 / 25.4
  when :a5
    @options[:paper_width] = 148 * 72 / 25.4
    @options[:paper_height] = 210 * 72 / 25.4
  when :b5
    @options[:paper_width] = 176 * 72 / 25.4
    @options[:paper_height] = 250 * 72 / 25.4
  when Array
    @options[:paper_width] = @options[:paper][0].to_f
    @options[:paper_height] = @options[:paper][1].to_f
  else
    raise ArgumentError, "Unknown paper size #{ @options[:paper] }"
  end
  
  case @options[:orientation]
  when :portrait
    @options[:page_width] = @options[:paper_width]
    @options[:page_height] = @options[:paper_height]
  when :landscape
    @options[:page_height] = @options[:paper_width]
    @options[:page_width] = @options[:paper_height]
  else
    raise ArgumentError, "Bad orientation #{@options[:orientation]}"
  end

  @options[:page_width] /= 2
  @options[:page_height] /= 2 if @options[:half_size]

  @options[:scale] = ( @options[:page_width] - ( @options[:margin] * 2.0 ) ) / @options[:page_width]

  pages_dir = File.join( dir, 'pages' )

  scratch_dir = File.join( dir, 'scratch' )
  directory scratch_dir

  web_dir = File.join( dir, 'web' )
  directory web_dir

  print_dir = File.join( dir, 'print' )
  directory print_dir

  layout_ps = File.join( scratch_dir, 'layout.ps' )

  task layout_ps => [ scratch_dir ]

  specials = Set.new
  pages = []

  if @options[:rasterize]
    print_ext = 'png'
  else
    print_ext = 'eps'
  end

  filelist = []

  if File.exists?( "pages.yaml" )
    current_page_number = 1
    YAML::load( File.open( "pages.yaml" ) ).each do |info|
      if info['blank']
        current_page_number += 1
      else
        name = File.basename( info['file'] )
        if matches = /^(.*)\.([^\.]*)$/.match(name)
          all, name, ext = matches.to_a
        end

        if info['spread'] == true
          page_file = "pages-%02d-%02d.%s" % [ current_page_number, current_page_number + 1, ext ]
          current_page_number += 2
        elsif info['page_name']
          page_file = "%s.%s" % [ info['page_name'].gsub("_","-"), ext ]
        else
          page_file = "page-%02d.%s" % [ current_page_number, ext ]
          current_page_number += 1
        end
        filelist << [ info['file'], page_file ]
      end
    end
  else
    FileList[File.join( pages_dir, '*.*' )].each do |page_file|
      filelist << [ page_file, page_file ]
    end
  end

  filelist.each do |original_image_file, page_file|
    name = File.basename( page_file )
    if matches = /^(.*)\.([^\.]*)$/.match(name)
      all, name, ext = matches.to_a
    end

    min = max = nil
    print_name = name
    case name
    when *SPECIAL_NAME_RES
      name = normalize_special_name( name )
      next unless SPECIAL_NAMES.include? name # warning?
      next if specials.include? name # warning?
      specials.add name
      if name == 'front-cover'
        web_name = "page-00"
      else
        web_name = nil
      end
    when /^page\W*(\d+)$/i
      min = max = $1.to_i
      suffix = "%02d" % [ min ]
      print_name = "page-#{ suffix }"
      web_name = print_name
    when /^pages?\W*(\d+)\D+(\d+)$/i
      min, max = $1.to_i, $2.to_i
      min, max = max, min if max < min
      suffix = "%02d-%02d" % [ min, max ]
      print_name = "pages-#{ suffix }"
      web_name = print_name
    else
      # warning?
      next
    end

    print_file = File.join( scratch_dir, "#{ print_name }.#{ print_ext }" )

    if min
      size = max - min + 1
      if min > 0
        (0...size).each do |n|
          pages[n+min-1] = ImageSlice[ n, size, print_file ]
        end
      end
    else
      size = 1
    end

    case ext
    when 'svg'
      case print_ext
      when 'png'
        print_png_from_svg( print_file, size, original_image_file )
      when 'eps'
        eps_from_svg( print_file, original_image_file )
      end
    else
      case print_ext
      when 'png'
        print_png_from_image_file( print_file, size, original_image_file )
      when 'eps'
        print_eps_from_image_file( print_file, size, original_image_file )
      end
    end
    task print_file => [ scratch_dir ]

    if web_name
      temp_page_png = File.join( scratch_dir, "#{ web_name }-web.png" )

      task temp_page_png => [ scratch_dir ]
      
      case @options[:web_format]
      when :jpg, :jpeg
        web_page_image = File.join( web_dir, "#{ web_name }.jpeg" )
      else
        web_page_image = File.join( web_dir, "#{ web_name }.png" )
        temp_page_png = web_page_image
      end

      task web_page_image => [ web_dir ]

      case ext
      when 'svg'
        web_png_from_svg( temp_page_png, original_image_file )
      else
        web_png_from_image_file( temp_page_png, original_image_file )
      end

      task :web => [ web_page_image ]

      if @options[:generate_thumbnails]
        thumbnail_name = web_name.sub( /^page/, 'thumbnail' )
        thumbnail_jpeg = File.join( web_dir, "#{ thumbnail_name }.jpeg" )
        thumbnail_jpeg_from_image( thumbnail_jpeg, temp_page_png )
        task thumbnail_jpeg => [ web_dir ]

        task :web => [ thumbnail_jpeg ]
      end
      case @options[:web_format]
      when :png
      when :jpg, :jpeg
        jpeg_from_image_file( web_page_image, temp_page_png )
      end
    end
  end

  inside_front = nil
  inside_back = nil
  if @options[:use_inside]
    inside_front = pages.shift
    inside_back = pages.pop if pages.size % 4 == 1
  end
  pages = [ nil, inside_front ] +
          pages + [ nil ] * ( ( 4 - pages.size ) % 4 ) +
          [ inside_back, nil ]

  specials.each do |name|
    filename = File.join( scratch_dir, "#{ name }.#{ print_ext }" )
    task filename => [ scratch_dir ]
    case name
    when 'front-cover'
      pages[0] ||= ImageSlice[ 0, 1, filename ] # 'cover' takes precedence
    when 'back-cover'
      pages[-1] ||= ImageSlice[ 0, 1, filename ] # 'cover' takes precedence
    when 'cover'
      pages[-1] = ImageSlice[ 0, 2, filename ]
      pages[0] = ImageSlice[ 1, 2, filename ]
    end
  end

  layout_booklet( layout_ps, *pages )

  duplex_ps = File.join( scratch_dir, "duplex.ps" )

  task duplex_ps => [ scratch_dir ]

  %w(proof back front duplex).each do |kind|
    case kind
    when 'duplex', 'proof'
      source = layout_ps
    else
      source = duplex_ps
    end 
    kind_ps = File.join( scratch_dir, "#{ kind }.ps" )
    send( "#{ kind }_ps", kind_ps, source )
    task kind_ps => [ scratch_dir ]
    kind_pdf = File.join( print_dir, "#{ kind }.pdf" )
    pdf_from_ps( kind_pdf, kind_ps )
    task kind_pdf => [ print_dir ]
    task kind => [ kind_pdf ]
  end

  desc "Generate PDF file for duplex printing"
  task :duplex

  desc "Generate PDF files for single-sided printing"
  task :single => [ :front, :back ]

  desc "Generate PDF file for review of reader spreads"
  task :proof

  desc "Remove all output and intermediate files"
  task :clean do
    rm_rf print_dir
    rm_rf web_dir
    rm_rf scratch_dir
  end

  desc "Generate graphics for the web"
  task :web

  desc "All print and web output"
  task :all => [ :proof, :duplex, :single, :web ]
end

end

def minicomic( dir, options={} )
  Minicomic.new( dir, options )
end

